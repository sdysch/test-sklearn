from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix, accuracy_score

def getAccuracy(nLeaf, X_train, X_test, y_train, y_test):
    model = DecisionTreeClassifier(max_leaf_nodes = nLeaf)
    model.fit(X_train, y_train)
    predictions = model.predict(X_test)

    # accuracy
    return accuracy_score(y_test, predictions)


# import iris dataset
iris = datasets.load_iris()
X    = iris.data
y    = iris.target

# split the training/testing data
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = 1)

leaves = [None, 2]

results = {leaf: getAccuracy(leaf, X_train, X_test, y_train, y_test) for leaf in leaves}
print results
